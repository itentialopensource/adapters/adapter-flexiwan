# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Flexiwan System. The API that was used to build the adapter for Flexiwan is usually available in the report directory of this adapter. The adapter utilizes the Flexiwan API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The flexiWAN adapter from Itential is used to integrate the Itential Automation Platform (IAP) with flexiWAN. With this adapter you have the ability to perform operations on items such as:

- Devices
- Routes
- Jobs
- Organizations

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
