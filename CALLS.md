## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for flexiWAN. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for flexiWAN.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the flexiWAN. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getTokens(offset, limit, org, callback)</td>
    <td style="padding:15px">Get all Tokens</td>
    <td style="padding:15px">{base_path}/{version}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTokens(org, body, callback)</td>
    <td style="padding:15px">Create new access token</td>
    <td style="padding:15px">{base_path}/{version}/tokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTokensId(id, org, callback)</td>
    <td style="padding:15px">Get all Tokens</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTokensId(id, org, callback)</td>
    <td style="padding:15px">Delete token</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTokensId(id, org, body, callback)</td>
    <td style="padding:15px">Modify a token</td>
    <td style="padding:15px">{base_path}/{version}/tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccesstokens(offset, limit, org, callback)</td>
    <td style="padding:15px">Get all AccessTokens</td>
    <td style="padding:15px">{base_path}/{version}/accesstokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccesstokens(org, body, callback)</td>
    <td style="padding:15px">Create new access token</td>
    <td style="padding:15px">{base_path}/{version}/accesstokens?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccesstokensId(id, org, callback)</td>
    <td style="padding:15px">Delete access token</td>
    <td style="padding:15px">{base_path}/{version}/accesstokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(offset, limit, org, callback)</td>
    <td style="padding:15px">Get all registered devices</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devicesLatestVersionsGET(callback)</td>
    <td style="padding:15px">Get devices latest available version</td>
    <td style="padding:15px">{base_path}/{version}/devices/latestVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devicesApplyPOST(org, body, callback)</td>
    <td style="padding:15px">Execute an action on the device side</td>
    <td style="padding:15px">{base_path}/{version}/devices/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devicesUpgdSchedPOST(org, body, callback)</td>
    <td style="padding:15px">Create new access token</td>
    <td style="padding:15px">{base_path}/{version}/devices/upgdSched?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesId(id, org, callback)</td>
    <td style="padding:15px">Get all registered devices</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicesId(id, org, callback)</td>
    <td style="padding:15px">Delete device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDevicesId(id, org, body, callback)</td>
    <td style="padding:15px">Modify device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesIdUpgdSched(id, org, body, callback)</td>
    <td style="padding:15px">Create new access token</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/upgdSched?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdConfiguration(id, org, callback)</td>
    <td style="padding:15px">Get Device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesIdDhcp(id, org, body, callback)</td>
    <td style="padding:15px">Add DHCP server</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdDhcp(id, offset, limit, org, callback)</td>
    <td style="padding:15px">Retrieve device DHCP information</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdDhcpDhcpId(id, dhcpId, org, callback)</td>
    <td style="padding:15px">Get DHCP by ID</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicesIdDhcpDhcpId(id, dhcpId, force = 'yes', org, callback)</td>
    <td style="padding:15px">Delete DHCP</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDevicesIdDhcpDhcpId(id, dhcpId, org, body, callback)</td>
    <td style="padding:15px">Modify DHCP</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevicesIdDhcpDhcpId(id, dhcpId, org, callback)</td>
    <td style="padding:15px">ReApply DHCP</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/dhcp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesIdApply(id, org, body, callback)</td>
    <td style="padding:15px">Execute an action on the device side</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnels(offset, limit, org, callback)</td>
    <td style="padding:15px">Retrieve device tunnels information</td>
    <td style="padding:15px">{base_path}/{version}/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelsIdDELETE(id, org, callback)</td>
    <td style="padding:15px">Delete a tunnel</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdRoutes(id, offset, limit, callback)</td>
    <td style="padding:15px">Retrieve device routes information</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevicesIdStaticroutes(id, org, body, callback)</td>
    <td style="padding:15px">Create new static route</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdStaticroutes(id, offset, limit, org, callback)</td>
    <td style="padding:15px">Retrieve device static routes information</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicesIdStaticroutesRoute(id, route, org, callback)</td>
    <td style="padding:15px">Delete static route</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/staticroutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDevicesIdStaticroutesRoute(id, route, org, body, callback)</td>
    <td style="padding:15px">Modify static route</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/staticroutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">devicesStatisticsGET(org, callback)</td>
    <td style="padding:15px">Retrieve devices statistics information</td>
    <td style="padding:15px">{base_path}/{version}/devices/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdStatistics(id, callback)</td>
    <td style="padding:15px">Retrieve device statistics information</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesIdLogs(id, offset, limit, filter, callback)</td>
    <td style="padding:15px">Retrieve device logs information</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobs(status = 'all', offset, limit, ids, org, callback)</td>
    <td style="padding:15px">Get all Jobs</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobs(org, body, callback)</td>
    <td style="padding:15px">Delete a job</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobsId(id, org, callback)</td>
    <td style="padding:15px">Get Job by ID</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobsId(id, org, callback)</td>
    <td style="padding:15px">Delete a job</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotifications(offset, limit, org, op, status, callback)</td>
    <td style="padding:15px">Get all Notifications</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNotifications(org, body, callback)</td>
    <td style="padding:15px">Modify notifications</td>
    <td style="padding:15px">{base_path}/{version}/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNotificationsId(id, org, body, callback)</td>
    <td style="padding:15px">Modify notification</td>
    <td style="padding:15px">{base_path}/{version}/notifications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMembers(offset, limit, callback)</td>
    <td style="padding:15px">Get all Members</td>
    <td style="padding:15px">{base_path}/{version}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMembers(body, callback)</td>
    <td style="padding:15px">Create new member</td>
    <td style="padding:15px">{base_path}/{version}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMembersId(id, callback)</td>
    <td style="padding:15px">Get Member</td>
    <td style="padding:15px">{base_path}/{version}/members/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMembersId(id, body, callback)</td>
    <td style="padding:15px">Modify member</td>
    <td style="padding:15px">{base_path}/{version}/members/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMembersId(id, callback)</td>
    <td style="padding:15px">Delete member</td>
    <td style="padding:15px">{base_path}/{version}/members/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">membersOptionsTypeGET(type, callback)</td>
    <td style="padding:15px">Get Member</td>
    <td style="padding:15px">{base_path}/{version}/members/options/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccounts(offset, limit, callback)</td>
    <td style="padding:15px">Get all Accounts</td>
    <td style="padding:15px">{base_path}/{version}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">accountsSelectPOST(body, callback)</td>
    <td style="padding:15px">Select account</td>
    <td style="padding:15px">{base_path}/{version}/accounts/select?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountsId(id, callback)</td>
    <td style="padding:15px">Retrieve account information</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountsId(id, body, callback)</td>
    <td style="padding:15px">Modify account information</td>
    <td style="padding:15px">{base_path}/{version}/accounts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizations(offset, limit, callback)</td>
    <td style="padding:15px">Get all organizations</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizations(body, callback)</td>
    <td style="padding:15px">Add new organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">organizationsSelectPOST(body, callback)</td>
    <td style="padding:15px">Select organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/select?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationsId(id, callback)</td>
    <td style="padding:15px">Get Organization by ID</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationsId(id, callback)</td>
    <td style="padding:15px">Delete organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrganizationsId(id, body, callback)</td>
    <td style="padding:15px">Modify organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInvoices(offset, limit, callback)</td>
    <td style="padding:15px">Get all invoices</td>
    <td style="padding:15px">{base_path}/{version}/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCoupons(body, callback)</td>
    <td style="padding:15px">Add new coupon</td>
    <td style="padding:15px">{base_path}/{version}/coupons?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathlabels(offset, limit, org, callback)</td>
    <td style="padding:15px">Get all Path labels</td>
    <td style="padding:15px">{base_path}/{version}/pathlabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPathlabels(org, body, callback)</td>
    <td style="padding:15px">Add a new Path label</td>
    <td style="padding:15px">{base_path}/{version}/pathlabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathlabelsId(id, org, callback)</td>
    <td style="padding:15px">Get a Path label by id</td>
    <td style="padding:15px">{base_path}/{version}/pathlabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPathlabelsId(id, org, body, callback)</td>
    <td style="padding:15px">Modify a Path label</td>
    <td style="padding:15px">{base_path}/{version}/pathlabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePathlabelsId(id, org, callback)</td>
    <td style="padding:15px">Delete a Path label</td>
    <td style="padding:15px">{base_path}/{version}/pathlabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
