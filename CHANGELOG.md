
## 0.3.9 [10-15-2024]

* Changes made at 2024.10.14_20:55PM

See merge request itentialopensource/adapters/adapter-flexiwan!24

---

## 0.3.8 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-flexiwan!22

---

## 0.3.7 [08-14-2024]

* Changes made at 2024.08.14_19:14PM

See merge request itentialopensource/adapters/adapter-flexiwan!21

---

## 0.3.6 [08-07-2024]

* Changes made at 2024.08.07_12:16PM

See merge request itentialopensource/adapters/adapter-flexiwan!20

---

## 0.3.5 [05-01-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!16

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_13:51PM

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!14

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!10

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_10:43AM

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!9

---

## 0.3.1 [02-26-2024]

* Changes made at 2024.02.26_13:06PM

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!8

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!6

---

## 0.2.1 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!7

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!3

---

## 0.1.3 [03-05-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!2

---

## 0.1.2 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-flexiwan!1

---

## 0.1.1 [05-20-2020]

- Initial Commit

See commit 8f4ad1d

---
